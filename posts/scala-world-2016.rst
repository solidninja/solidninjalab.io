.. title: scala.world conference notes
.. slug: scala-world-2016
.. date: 2016-09-21 10:00:00 UTC
.. tags: schedule, scala, scala.world
.. category: conference
.. link: 
.. description: 
.. type: text
.. author: Vladimir Lushnikov

.. thumbnail:: /galleries/scala-world-2016/22_scala-world-2016.jpg
    :alt: #scalahike
    :align: center

    Scala Hikers @ `Scarfell Pike`_ - `see here for more <link://gallery/scala-world-2016>`__ 

I attended the `scala.world`_ conference that took place in the Lake District between the 12th and 13th September 2016. These are some notes on the conference, mainly for the benefit of my colleagues but also as a convenient way to start off the SOLID Ninja blog.

The hiking
----------

Notes on scala.world would not be complete without mentioning the hiking that took place over the preceding weekend. The Lake District weather gods were favourable throughout the conference which was unusual to say the least (at least if you live in England). On the Saturday there was an organized hike to `Scarfell Pike`_ which is the tallest point in England and on Sunday there was a choice of cycling and hiking again. Needless to say even the "easy" route does not look like a bike ride through the Netherlands. `Photos are here. <link://gallery/scala-world-2016>`__

The hiking was a great way to meet people and talk about life rather than `SI-2712`_...

The conference
--------------

The schedule was finally up on Monday morning about 4am - so the conference could begin!

In no particular order, these are the talks/workshops I made notes on:


Martin's Keynote
~~~~~~~~~~~~~~~~

Martin Odersky was up first with a keynote titled `Compilers are (in memory) databases` (which admittedly has been given before so check below for the YouTube video). It is very nice to see that dotc_ has got lots of small, reasonable passes in the compiler rather than a few big monolithic passes.

.. youtube:: WxyyJyB_Ssc


CBT
~~~

CBT_ is in my opinion the **S**\(imple)\ **B**\(uild)\ **T**\(ool) that should have been - Christian did a great talk explaining how cbt emerged from the design of SBT_. The slides are not yet up - here is the best I could do:

.. slides::

    /galleries/scala-world-2016/61_scala-world-2016.jpg
    /galleries/scala-world-2016/62_scala-world-2016.jpg
    /galleries/scala-world-2016/63_scala-world-2016.jpg

CBT looks very promising because:

+ The model is much simpler than SBT so you do not need to know about `scope axes`_

  + I doubt anyone will be writing a book__ about cbt though...

__ sbt-in-action_

+ It's fast! (both to startup and resolve dependencies)

  + No ivy involvement helps a lot in this respect, though support for coursier_ is currently not in a good shape

+ There is no DSL - everything is a project and tasks are functions, so you can write your own plugins etc. the same way you always write code (in Scala)

Generic derivation 
~~~~~~~~~~~~~~~~~~

I did not go to this one, but Travis Brown did an interesting talk on `Generic derivation`_ (in Circe__, which is a `fast json library`__)

__ circe-derivation_
__ circe_

The Type Astronaut's Guide to Shapeless
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

With a title like that it was going to be hard to disappoint, and Dave Gurnell's talk about shapeless_ was very good:

.. slides::

    /galleries/scala-world-2016/64_scala-world-2016.jpg
    /galleries/scala-world-2016/65_scala-world-2016.jpg
    /galleries/scala-world-2016/66_scala-world-2016.jpg
    /galleries/scala-world-2016/67_scala-world-2016.jpg

The worked example about doing automated case class migration (using ``HList`` union and intersection) in particular was both useful and easy to follow.

`The type astronaut's guide`_ is currently in development on GitHub.

A whirlwind tour of scala.meta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ólafur Páll Geirsson presented a workshop on using `scala.meta`_ to automatically transform scala programs (in one example, by replacing usages of ``Throwable`` with the ``NonFatal(_)`` extractor):

.. youtube:: -l7pV0sFq1c

At this point the Internet access at the workshop was not very good, but luckily the workshop is available here: `scala.meta-workshop`_

The `Metaprogramming 2.0`_ talk is worth watching as well.

Talks about the ``Free`` Monad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There were a few talks/workshops on the `Free Monad`_ which is (and has been) a fairly hot topic in the past year.

Raul Raja presented a talk `Run wild, Run free`_ which gives a very natural exploration of the problem of composing your computations starting with ``for``-comprehensions and exploring until you arrive at the free monad.

Pawel Szulc also gave a workshop on free monads: `make-your-programs-free`_. The core idea is that while actually implementing the interpreter you get to a point where you are wrapping higher-level operations with low-level 'plumbing' code. One way of solving this problem is by definining an interpreter from a higher-level algebra to a lower-level algebra and implementing them separately (which you can see if you follow the commits in the workshop). 

There were plenty of mentions to libraries that help you with free monads: [1]_ [2]_ and it would not be fair to not talk about the downsides of ``Free`` as well [3]_.

Tuesday Keynote
~~~~~~~~~~~~~~~

The keynote on Tuesday was Dick Wall and Josh Suereth talking about all the uses of ``for``\s: `Use the fors, Luke`_. It started off a super friendly to beginners and at the end also addressed the reasons why perhaps a ``for`` is not the ultimate tool for Monad composition (shhh, that's a lie). Unfortunately slides/video are not up yet.

Managing your Resources
~~~~~~~~~~~~~~~~~~~~~~~

This talk was one of the highlights of the conference for me I think - resource management is one of the problems in Scala that is difficult to solve because of a lack of a `lifetime system`_ as found in Rust_ say. And while there are more elegant ways to ``close()`` your resources after yourself, most of the solutions require either some wrapping construct (such as a ``for`` or a monad or a simple loan-pattern style closure). It is rare to see such simplicity (in Scala):

.. youtube:: MV2eJkwarT4

`The presentation is available`__ but it's in Keynote format which GitHub does not have a reader for.

__ https://github.com/densh/talks/blob/master/2016-09-XX-scoped-resource-management.key

Here is a working example from the presentation:

.. gist:: 1348bfb98928ba10ab18c5d7b51ee87f

ScalaFiddle
~~~~~~~~~~~

Scala officially entered the stage of languages with an online interactive sandbox, called ScalaFiddle_. Play with it - it has decent highlighting, speed and libraries that you can use just by clicking and importing.


Tales from Compiling to the JVM
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This was an interesting talk by Lukas Rytz about certain things that had to change in the Scala 2.12 compiler because of Java 8. Like not initializing static final variables outside the class initialization block... Talk is not up yet unfortunately.

Typelevel hackday
-----------------

The third day of the conference was a Typelevel_ hackday which started off with a few talks in the morning. Of these I only took some notes for two of them:

+ `Cats in London`_ - this was Pawel Szulc attempting the what must be by-now infamous interview problem of the Checkout with apples and oranges using Cats_ and the ``Free`` monad
+ `Array-based collections`_ - was a nice talk about a collection library that is very different - these ones are immutable and backed by a single flat array

Other References
----------------

+ John de Goes ran an `Advanced Functional Programming with Scala`_ training workshop for 2 days prior to the conference and the linked notes look great
+ Monix_ was mentioned as a library quite often and I was not fully aware of it before coming to the conference

So here is the presentation from flatMap Oslo 2016 talking about it:

.. vimeo:: 165922572

It was an excellent conference and I look forward to scala.world 2017!

.. _`scala.world`: https://scala.world
.. _Scarfell Pike: https://en.wikipedia.org/wiki/Scafell_Pike
.. _SI-2712: https://issues.scala-lang.org/browse/SI-2712
.. _dotc: https://github.com/lampepfl/dotty/blob/master/src/dotty/tools/dotc/Compiler.scala
.. _CBT: https://github.com/cvogt/cbt
.. _SBT: http://www.scala-sbt.org/
.. _`scope axes`: http://www.scala-sbt.org/release/docs/Scopes.html
.. _sbt-in-action: https://www.manning.com/books/sbt-in-action
.. _coursier: https://github.com/alexarchambault/coursier
.. _`Generic derivation`: https://meta.plasm.us/slides/scalaworld/#1 
.. _circe-derivation: https://github.com/travisbrown/circe-derivation
.. _circe: https://github.com/travisbrown/circe
.. _shapeless: https://github.com/milessabin/shapeless
.. _`The type astronaut's guide`: https://github.com/davegurnell/shapeless-guide/blob/develop/dist/shapeless-guide.pdf
.. _`scala.meta`: http://scalameta.org/
.. _`scala.meta-workshop`: https://olafurpg.github.io/scala.meta-workshop/
.. _`Metaprogramming 2.0`: https://www.youtube.com/watch?v=IPnd_SZJ1nM
.. _`Free Monad`: http://underscore.io/blog/posts/2015/04/14/free-monads-are-simple.html
.. _`Run wild, Run free`: https://speakerdeck.com/raulraja/run-wild-run-free
.. _`make-your-programs-free`: https://github.com/rabbitonweb/make-your-programs-free
.. _`Use the fors, Luke`: https://github.com/dickwall/use-the-fors-luke
.. _`lifetime system`: https://doc.rust-lang.org/book/lifetimes.html
.. _Rust: https://www.rust-lang.org
.. _ScalaFiddle: https://scalafiddle.io/sf/n6lR8Xh/2
.. _Typelevel: http://typelevel.org
.. _`Cats`: https://github.com/typelevel/cats
.. _`Cats in London`: https://github.com/rabbitonweb/cats_toolbox/blob/master/src/main/scala/ct/Checkout.scala
.. _`Array-based collections`: https://github.com/rklaehn/abc
.. _`Advanced Functional Programming with Scala`: https://gist.github.com/jdegoes/97459c0045f373f4eaf126998d8f65dc
.. _Monix: https://monix.io

.. [1] https://github.com/typelevel/cats/blob/master/docs/src/main/tut/freemonad.md
.. [2] http://perevillega.com/freek-and-free-monads
.. [3] http://www.slideshare.net/KelleyRobinson1/why-the-free-monad-isnt-free-61836547

